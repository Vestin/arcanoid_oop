# Opis
Arkanoid zaimplementowany we względnie rudymentarny sposób.

Autor: Jarosław Surówka

# Sterowanie
##Ekran gry:
* SPACJA - puść "kulkę"
* LEWA/PRAWA STRZAŁKA - przesuń paletkę

##Ekran high scores:
* LEWA/PRAWA STRZAŁKA - wybierz literę
* GÓRA/DÓŁ STRZAŁKA - zmień literę
* ENTER - zatwierdź wpis
* ESC - wyjdź
