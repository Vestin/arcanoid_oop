package oop.arkanoid;

import com.badlogic.gdx.math.Vector2;

class Boundary {
    enum Orientation {
        Horizontal, Vertical
    }

    enum ForbiddenSide {
        Lesser, Greater
    }

    private final int threshold;
    private final Orientation orientation;
    private final ForbiddenSide forbiddenSide;

    Boundary(Orientation orientation, ForbiddenSide forbiddenSide, int threshold) {
        this.orientation = orientation;
        this.forbiddenSide = forbiddenSide;
        this.threshold = threshold;
    }

    boolean isHorizontal() {
        return orientation == Orientation.Horizontal;
    }

    Vector2 getConstrainVector(GameDrawable object) {
        Vector2 constrainVector = new Vector2(0.0f, 0.0f);
        if (this.orientation == Orientation.Vertical) {
            if (this.forbiddenSide == ForbiddenSide.Lesser) {
                constrainVector.add(
                        Math.max(0.0f, this.threshold - Math.min(object.getLeft(), object.getRight())), 0.0f);
            } else {
                constrainVector.add(
                        Math.min(0.0f, this.threshold - Math.max(object.getLeft(), object.getRight())), 0.0f);
            }
        } else {
            if (this.forbiddenSide == ForbiddenSide.Lesser) {
                constrainVector.add(
                        0.0f,
                        Math.max(0.0f, this.threshold - Math.min(object.getDown(), object.getUp())));
            } else {
                constrainVector.add(0.0f,
                        Math.min(0.0f, this.threshold - Math.max(object.getDown(), object.getUp())));
            }
        }
        return constrainVector;
    }

    Vector2 getBounceOffVector(GameDrawable object) {
        return getConstrainVector(object).scl(2);
    }
}
