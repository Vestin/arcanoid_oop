package oop.arkanoid;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class ArkanoidGame extends ApplicationAdapter {
    private GameState gameState;

    @Override
    public void create() {
        Gdx.gl.glClearColor(0, 0, 10, 1);
        changeState(new GameplayState(this));
    }

    @Override
    public void render() {
        gameState.update();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gameState.draw();
    }

    @Override
    public void dispose() {
    }

    void changeState(GameState gameState) {
        this.gameState = gameState;
        InputHandler inputHandler = new InputHandler(gameState);
        Gdx.input.setInputProcessor(inputHandler);
    }
}
