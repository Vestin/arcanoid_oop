package oop.arkanoid;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

abstract class GameDrawable {
    Vector2 position;
    Vector2 size;
    final static GameplayTextureManager textureMan = new GameplayTextureManager();

    @SuppressWarnings("unused")
    abstract void update(long delta);

    void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(getTexture(), position.x, position.y, size.x, size.y);
    }

    abstract Texture getTexture();

    Vector2 getPosition() {
        return position;
    }

    float getLeft() {
        return position.x;
    }

    float getRight() {
        return position.x + size.x;
    }

    float getDown() {
        return position.y;
    }

    float getUp() {
        return position.y + size.y;
    }

    void setPosition(Vector2 position) {
        this.position = position;
    }

    Vector2 getSize() {
        return size;
    }
}
