package oop.arkanoid;

class ScoreManager {
    private int score = 0;

    enum ScoreEvent {
        WALL_BOUNCE(1), PADDLE_BOUNCE(10), BRICK_DESTROYED(100), BALL_LOST(-1000);

        private final int points;

        ScoreEvent(int points) {
            this.points = points;
        }

        int getPoints() {
            return points;
        }
    }

    void updateScore(ScoreEvent scoreEvent) {
        score += scoreEvent.getPoints();
    }

    int getScore() {
        return score;
    }
}
