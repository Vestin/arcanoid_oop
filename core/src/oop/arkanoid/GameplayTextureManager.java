package oop.arkanoid;

import com.badlogic.gdx.graphics.Texture;

@SuppressWarnings("unused")
final class GameplayTextureManager {
    private Texture ballTexture;
    private Texture paddleTexture;
    private Texture brickTexture;

    Texture getTexture(final Ball brick) {
        if (ballTexture == null) ballTexture = new Texture("ball.png");
        return ballTexture;
    }

    Texture getTexture(final Paddle paddle) {
        if (paddleTexture == null) paddleTexture = new Texture("paddle.png");
        return paddleTexture;
    }

    Texture getTexture(final Brick brick) {
        if (brickTexture == null) brickTexture = new Texture("brick.png");
        return brickTexture;
    }

    void freeTextures() {
        ballTexture.dispose();
        paddleTexture.dispose();
        brickTexture.dispose();
    }
}
