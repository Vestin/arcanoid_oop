package oop.arkanoid;

public class GameplayState implements GameState {
    private final ArkanoidGame stateMachine;
    private final Board board;
    private final Timer timer;
    private final ScoreManager scoreManager;

    GameplayState(ArkanoidGame stateMachine) {
        this.stateMachine = stateMachine;
        scoreManager = new ScoreManager();
        board = new Board(scoreManager, this);
        timer = new Timer();
    }

    @Override
    public void resolveInput(RecognizedKeys key, boolean isDown) {
        switch (key) {
            case LEFT: {
                if (isDown) {
                    for (Paddle paddle : board.getPaddles()) {
                        paddle.turnLeftThrusterOn();
                    }
                } else {
                    for (Paddle paddle : board.getPaddles()) {
                        paddle.turnLeftThrusterOff();
                    }
                }
                break;
            }
            case RIGHT: {
                if (isDown) {
                    for (Paddle paddle : board.getPaddles()) {
                        paddle.turnRightThrusterOn();
                    }
                } else {
                    for (Paddle paddle : board.getPaddles()) {
                        paddle.turnRightThrusterOff();
                    }
                }
                break;
            }
            case SPACE: {
                board.launchBall();
                break;
            }
            case NULL:
            default:
                break;
        }
    }

    @Override
    public void update() {
        board.update(timer.getDelta());
    }

    @Override
    public void draw() {
        board.draw();
    }

    void showHighscoreScreen() {
        HighscoreState highscoreState = new HighscoreState(scoreManager.getScore());
        stateMachine.changeState(highscoreState);
        GameDrawable.textureMan.freeTextures();
    }
}
