package oop.arkanoid;

interface GameState {
    void resolveInput(RecognizedKeys key, boolean isDown);

    void update();

    void draw();
}
