package oop.arkanoid;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

class Ball extends GameDrawable {
    private static final Vector2 initialImmobility = new Vector2(0.0f, 0.0f);
    private static final Vector2 initialVelocity = new Vector2(0.25f, 0.25f);
    private static final int normalDamage = 1;
    private Vector2 velocity;

    private final GameDrawable paddlePrimeHandle;

    Ball(GameDrawable paddle) {
        size = new Vector2(15.0f, 15.0f);
        position = getBallPositionFromPaddle(paddle);
        velocity = new Vector2(initialImmobility);
        paddlePrimeHandle = paddle;
    }

    Vector2 getVelocity() {
        return velocity;
    }

    int getDamage() {
        return normalDamage;
    }

    public void update(long delta) {
        if (!velocity.isZero()) {
            position.add(velocity.x * delta, velocity.y * delta);
        } else {
            position = getBallPositionFromPaddle(paddlePrimeHandle);
        }
    }

    @Override
    Texture getTexture() {
        return textureMan.getTexture(this);
    }

    private Vector2 getBallPositionFromPaddle(GameDrawable paddle) {
        float verticalOffset = 2;
        Vector2 ballPosition =
                new Vector2(paddle.position.x + (paddle.size.x / 2), paddle.position.y + paddle.size.y);
        ballPosition.add(-size.x / 2, verticalOffset);
        return ballPosition;
    }

    void putInMotion() {
        if (velocity.isZero()) {
            velocity = new Vector2(initialVelocity);
        }
    }

    void reflect(boolean horizontalAxis) {
        if (horizontalAxis) {
            velocity.scl(1.0f, -1.0f);
        } else {
            velocity.scl(-1.0f, 1.0f);
        }
    }
}
