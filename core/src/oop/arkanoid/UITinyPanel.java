package oop.arkanoid;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;

class UITinyPanel extends GameDrawable {
    private final BitmapFont uiFont;
    private static final Color uiTextColor = Color.valueOf("#90FF50C0");
    private String uiText;
    private final Board board;

    UITinyPanel(Board board) {
        size = new Vector2(100.0f, 50.0f);
        position = new Vector2(550.0f, 50.0f);
        uiFont = new BitmapFont();
        uiFont.setColor(uiTextColor);
        uiText = "";
        this.board = board;
    }

    @Override
    void update(long delta) {
        uiText = String.format("Balls: %d\nScore: %d", board.getBallCount(), board.scoreManager.getScore());
    }

    @Override
    void draw(SpriteBatch spriteBatch) {
        uiFont.draw(spriteBatch, uiText, position.x, position.y, size.x, Align.topLeft, false);
    }

    @Override
    Texture getTexture() {
        return null;
    }
}
