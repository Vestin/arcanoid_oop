package oop.arkanoid;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

class Brick extends GameDrawable {
    private static final Vector2 standardSize = new Vector2(40.0f, 10.0f);

    private static final int maxHP = 1;
    private int currentHP;

    Brick(Vector2 position) {
        this.size = standardSize;
        this.position = position;
        this.currentHP = maxHP;
    }

    static Vector2 getStandardSize() {
        return standardSize;
    }


    @Override
    void update(long delta) {

    }

    @Override
    Texture getTexture() {
        return textureMan.getTexture(this);
    }

    void getHit(int damage) {
        this.currentHP -= damage;
    }

    boolean isDead() {
        return this.currentHP <= 0;
    }
}
