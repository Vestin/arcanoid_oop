package oop.arkanoid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.io.*;
import java.util.ArrayList;

class HighscoreState implements GameState {

    private static final String defaultName = "AAA";
    private static final String scorefileFilename = "scores.dat";
    private boolean entryFinalized;
    private int selectedLetterPosition;
    private int lastGameEntryRow;
    private final HighScoreEntry lastGameHighScore;

    private static final int numberOfEntries = 5;
    private final ArrayList<HighScoreEntry> highScoreEntries;

    private final SpriteBatch spriteBatch = new SpriteBatch();
    private final BitmapFont uiFont = new BitmapFont();
    private static final Color uiTextColor = Color.valueOf("#CEDD10FF");
    private static final Color uiSelectionColor = Color.valueOf("#EA339AFF");


    HighscoreState(int score) {
        entryFinalized = true;
        selectedLetterPosition = 0;
        lastGameHighScore = new HighScoreEntry(score, defaultName);
        uiFont.setColor(uiTextColor);
        uiFont.setFixedWidthGlyphs("01234567890QWERTYUIOPASDFGHJKLZXCVBNM:-_ ");
        highScoreEntries = new ArrayList<HighScoreEntry>();
        readScoresFromFile();
    }

    private void readScoresFromFile() {

        File scorefile = new File(scorefileFilename);

        try {
            if (!scorefile.exists()) {
                createDummyScorefile();
            }
            retrieveScorefileEntries();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void retrieveScorefileEntries() throws IOException {
        FileReader scorefileReader = new FileReader(HighscoreState.scorefileFilename);
        BufferedReader bufferedScorefileReader = new BufferedReader(scorefileReader);

        String currentHighscoreEntryText;
        int currentRow = 0;
        while ((currentHighscoreEntryText = bufferedScorefileReader.readLine()) != null
                && highScoreEntries.size() < numberOfEntries) {
            String[] currentHighscoreEntryArray = currentHighscoreEntryText.split(",");
            if (currentHighscoreEntryArray.length == 2) {
                HighScoreEntry currentHighscoreEntry =
                        new HighScoreEntry(Integer.parseInt(currentHighscoreEntryArray[1]),
                                currentHighscoreEntryArray[0]);
                if (currentHighscoreEntry.getScore() > lastGameHighScore.getScore()) {
                    highScoreEntries.add(currentHighscoreEntry);
                } else if (entryFinalized) {
                    entryFinalized = false;
                    lastGameEntryRow = currentRow;
                    highScoreEntries.add(lastGameHighScore);
                    if (highScoreEntries.size() < numberOfEntries) {
                        highScoreEntries.add(currentHighscoreEntry);
                    }
                }
            }
            currentRow++;
        }

        bufferedScorefileReader.close();
    }

    private void createDummyScorefile() throws IOException {
        final String dummyHighScoreContent = "BOB,2000\nJIM,1290\nANN,700\nREE,404\nLOL,-5000";

        FileWriter scorefileWriter = new FileWriter(HighscoreState.scorefileFilename);
        BufferedWriter bufferedScorefileWriter = new BufferedWriter(scorefileWriter);

        bufferedScorefileWriter.write(dummyHighScoreContent);

        bufferedScorefileWriter.close();
    }

    private void writeNewScorefile() {
        try {
            FileWriter scorefileWriter = new FileWriter(scorefileFilename);
            BufferedWriter bufferedScorefileWriter = new BufferedWriter(scorefileWriter);

            bufferedScorefileWriter.write(getScorefileText());

            bufferedScorefileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getScorefileText() {
        StringBuilder stringBuilder = new StringBuilder();
        for (HighScoreEntry highScoreEntry : highScoreEntries) {
            stringBuilder.append(highScoreEntry.getName());
            stringBuilder.append(",");
            stringBuilder.append(highScoreEntry.getScore());
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }

    @Override
    public void resolveInput(RecognizedKeys key, boolean isDown) {
        if (key == RecognizedKeys.ESC) {
            Gdx.app.exit();
        }

        if (!entryFinalized && isDown) {
            switch (key) {
                case LEFT: {
                    letterLeft();
                    break;
                }
                case RIGHT: {
                    letterRight();
                    break;
                }
                case UP: {
                    lastGameHighScore.decrementLetter(selectedLetterPosition);
                    break;
                }
                case DOWN: {
                    lastGameHighScore.incrementLetter(selectedLetterPosition);
                    break;
                }
                case ENTER: {
                    finalizeEntry();
                    break;
                }
                case NULL:
                default:
                    break;
            }
        }
    }

    private void letterRight() {
        if (selectedLetterPosition < 2) {
            selectedLetterPosition += 1;
        }
    }

    private void letterLeft() {
        if (selectedLetterPosition > 0) {
            selectedLetterPosition -= 1;
        }
    }

    private void finalizeEntry() {
        entryFinalized = true;
        writeNewScorefile();
    }

    @Override
    public void update() {

    }

    @Override
    public void draw() {
        spriteBatch.begin();


        uiFont.setColor(uiTextColor);
        uiFont.draw(spriteBatch, getBannerText(), 215, 400);
        uiFont.draw(spriteBatch, getHighscoreText(), 200, 300);
        if (!entryFinalized) {
            uiFont.setColor(uiSelectionColor);
            uiFont.draw(spriteBatch, getUnderlineString(), 200, 300);
        }

        spriteBatch.end();
    }

    private String getBannerText() {
        if (entryFinalized) return "Highscores";
        else return "New High Score!";
    }

    private String getHighscoreText() {
        StringBuilder stringBuilder = new StringBuilder();
        for (HighScoreEntry highScoreEntry : highScoreEntries) {
            stringBuilder.append(highScoreEntry.getName());
            stringBuilder.append(" : ");
            stringBuilder.append(highScoreEntry.getScore());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    private String getUnderlineString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < lastGameEntryRow; i++) {
            stringBuilder.append('\n');
        }
        for (int i = 0; i < selectedLetterPosition; i++) {
            stringBuilder.append(' ');
        }
        stringBuilder.append('_');
        return stringBuilder.toString();
    }
}
