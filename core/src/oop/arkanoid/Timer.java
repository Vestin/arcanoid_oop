package oop.arkanoid;

import com.badlogic.gdx.utils.TimeUtils;

class Timer {
    private long lastTime;

    Timer() {
        lastTime = TimeUtils.millis();
    }

    long getDelta() {
        long now = TimeUtils.millis();
        long deltaTime = now - lastTime;
        if (deltaTime > 0) {
            lastTime = now;
        }
        return deltaTime;
    }
}
