package oop.arkanoid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

class Board {
    private final List<Boundary> borders;
    private final Boundary ballCatcher;
    private final List<Paddle> paddles;
    private final List<Ball> balls;
    List<Brick> bricks;

    private static final int startBallCount = 3;
    private int ballCount;
    boolean ballLaunched;

    private final CollisionHandler collisionHandler;
    final ScoreManager scoreManager;
    private final UITinyPanel uiTinyPanel;
    private final GameplayState gameState;
    private final SpriteBatch spriteBatch;

    Board(ScoreManager scoreManager, GameplayState gameState){
        borders = new ArrayList<Boundary>();
        final int maxRight = Gdx.graphics.getWidth();
        final int maxUp = Gdx.graphics.getHeight();

        final Boundary topBorder =
                new Boundary(Boundary.Orientation.Horizontal, Boundary.ForbiddenSide.Greater, maxUp);
        borders.add(topBorder);
        final Boundary leftBorder =
                new Boundary(Boundary.Orientation.Vertical, Boundary.ForbiddenSide.Lesser, 0);
        borders.add(leftBorder);
        final Boundary rightBorder =
                new Boundary(Boundary.Orientation.Vertical, Boundary.ForbiddenSide.Greater, maxRight);
        borders.add(rightBorder);
        ballCatcher =
                new Boundary(Boundary.Orientation.Horizontal, Boundary.ForbiddenSide.Lesser, -10);

        paddles = new ArrayList<Paddle>();
        Paddle primaryPaddle = new Paddle();
        paddles.add(primaryPaddle);

        balls = new ArrayList<Ball>();
        resetBall();

        initBricks();

        ballCount = startBallCount;
        ballLaunched = false;

        collisionHandler = new CollisionHandler(this);

        this.scoreManager = scoreManager;

        uiTinyPanel = new UITinyPanel(this);

        this.gameState = gameState;

        spriteBatch = new SpriteBatch();
    }

    private void initBricks(){
        final int brickRows = 5;
        final int boardWidth = Gdx.graphics.getWidth();
        final int boardHeight = Gdx.graphics.getHeight();

        bricks = new ArrayList<Brick>();

        for (int y=0; y<brickRows; y++){
            for (int x=0; x+Brick.getStandardSize().x <= boardWidth; x+=Brick.getStandardSize().x){
                if (x==0 && y%2==1){
                    x += Brick.getStandardSize().x/2;
                }

                Vector2 position = new Vector2(x, boardHeight - (y+1)*Brick.getStandardSize().y);
                Brick curBrick = new Brick(position);
                bricks.add(curBrick);
            }
        }
    }

    List<Paddle> getPaddles(){
        return paddles;
    }

    List<Ball> getBalls(){
        return balls;
    }

    List<Boundary> getBorders() {
        return borders;
    }

    Boundary getBallCatcher(){
        return ballCatcher;
    }

    List<Brick> getBricks() {
        return bricks;
    }

    int getBallCount(){
        return ballCount;
    }

    private void checkBallsLost(){
        if (balls.isEmpty()){
            ballCount -= 1;
            // Give the player the next spare ball
            if (!isGameOverLoss()){
                resetBall();
            }
        }
    }

    private void checkGameOver() {
        if (isGameOverLoss() || isGameOverVictory()){
            this.gameState.showHighscoreScreen();
            spriteBatch.dispose();
        }
    }

    private boolean isGameOverVictory(){
        return bricks.isEmpty();
    }

    private boolean isGameOverLoss(){
        return ballCount <= 0;
    }

    private void resetBall(){
        if (!paddles.isEmpty()){
            Paddle paddlePrime = paddles.get(0);
            ballLaunched = false;
            Ball newBall = new Ball(paddlePrime);
            balls.add(newBall);
        }

    }

    void launchBall(){
        if (!ballLaunched && !balls.isEmpty()){
            balls.get(0).putInMotion();
            ballLaunched = true;
        }
    }

    void update(long delta) {
        for (Paddle paddle : paddles) paddle.update(delta);
        for (Ball ball : balls) ball.update(delta);
        collisionHandler.handleCollisions();
        checkBallsLost();
        uiTinyPanel.update(delta);
        checkGameOver();
    }

    void draw(){
        spriteBatch.begin();
        for (Paddle paddle : paddles) paddle.draw(spriteBatch);
        for (Ball ball : balls) ball.draw(spriteBatch);
        for (Brick brick : bricks) brick.draw(spriteBatch);
        uiTinyPanel.draw(spriteBatch);
        spriteBatch.end();
    }

}
