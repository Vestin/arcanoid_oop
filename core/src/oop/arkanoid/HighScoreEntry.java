package oop.arkanoid;

class HighScoreEntry {
    private final int score;
    private String name;

    HighScoreEntry(int score, String name) {
        this.score = score;
        this.name = name;
    }

    int getScore() {
        return score;
    }

    String getName() {
        return name;
    }

    void incrementLetter(int position) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            if (i == position) {
                char changedChar = name.charAt(i);
                if (changedChar == 'Z') {
                    changedChar = 'A';
                } else {
                    changedChar = (char) (changedChar + 1);
                }
                stringBuilder.append(changedChar);
            } else {
                stringBuilder.append(name.charAt(i));
            }
        }
        this.name = stringBuilder.toString();
    }

    void decrementLetter(int position) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            if (i == position) {
                char changedChar = name.charAt(i);
                if (changedChar == 'A') {
                    changedChar = 'Z';
                } else {
                    changedChar = (char) (changedChar - 1);
                }
                stringBuilder.append(changedChar);
            } else {
                stringBuilder.append(name.charAt(i));
            }
        }
        this.name = stringBuilder.toString();
    }
}
