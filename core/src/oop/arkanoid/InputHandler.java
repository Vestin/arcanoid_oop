package oop.arkanoid;

import com.badlogic.gdx.InputProcessor;

class InputHandler implements InputProcessor {
    private final GameState targetGameState;

    InputHandler(GameState targetGameState) {
        this.targetGameState = targetGameState;
    }

    @Override
    public boolean keyDown(int keycode) {
        targetGameState.resolveInput(RecognizedKeys.getEnum(keycode), true);
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        targetGameState.resolveInput(RecognizedKeys.getEnum(keycode), false);
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        return true;
    }
}
