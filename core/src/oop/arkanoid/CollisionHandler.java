package oop.arkanoid;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class CollisionHandler {
    private final Board board;

    private final HashSet<Brick> horizontalOverlapBricks;

    CollisionHandler(Board board) {
        this.board = board;
        horizontalOverlapBricks = new HashSet<Brick>();
    }

    void handleCollisions() {
        constrainPaddle();
        if (board.ballLaunched) {
            bounceBallOffWalls();
            bounceBallOffPaddle();
            checkBrickCollisions();
            catchBalls();
        }
    }

    private void constrainPaddle() {
        for (Paddle paddle : board.getPaddles()) {
            for (Boundary border : board.getBorders()) {
                Vector2 paddlePosition = paddle.getPosition();
                Vector2 constrainVector = border.getConstrainVector(paddle);
                paddle.setPosition(paddlePosition.add(constrainVector));
            }
        }
    }

    private void bounceBallOffWalls() {
        for (Ball ball : board.getBalls()) {
            for (Boundary border : board.getBorders()) {
                Vector2 ballPosition = ball.getPosition();
                Vector2 bounceVector = border.getBounceOffVector(ball);
                if (!bounceVector.isZero()) {
                    ball.setPosition(ballPosition.add(bounceVector));
                    ball.reflect(border.isHorizontal());
                    board.scoreManager.updateScore(ScoreManager.ScoreEvent.WALL_BOUNCE);
                }
            }
        }
    }

    private void bounceBallOffPaddle() {
        for (Ball ball : board.getBalls()) {
            for (Paddle paddle : board.getPaddles())
                if (ballObjectOverlap(ball, paddle, true)
                        && ballObjectOverlap(ball, paddle, false)
                        && ball.getVelocity().y < 0) {
                    float heightDifference = ball.getPosition().y - (paddle.getPosition().y + paddle.getSize().y);
                    if (heightDifference < ball.getSize().y / 2) {
                        ball.setPosition(new Vector2(ball.getPosition().x, ball.getPosition().y + heightDifference));
                        ball.reflect(true); // Always bounce ball horizontally (up) for gameplay reasons :)
                        board.scoreManager.updateScore(ScoreManager.ScoreEvent.PADDLE_BOUNCE);
                    }
                }
        }
    }

    private void checkBrickCollisions() {
        ArrayList<Brick> deadBricks = new ArrayList<Brick>();

        for (Ball ball : board.getBalls()) {
            for (Brick brick : board.getBricks()) {
                boolean horizontalPre = getHorizontalBrickPreCollision(brick);
                boolean verticalPost = ballObjectOverlap(ball, brick, false);
                boolean horizontalPost = ballObjectOverlap(ball, brick, true);

                if (verticalPost && horizontalPost) {
                    brick.getHit(ball.getDamage());
                    if (brick.isDead()) {
                        deadBricks.add(brick);
                    }
                    fixOverlap(ball, brick, !horizontalPre);
                    ball.reflect(horizontalPre);
                } else {
                    if (horizontalPost) {
                        horizontalOverlapBricks.add(brick);
                    } else {
                        horizontalOverlapBricks.remove(brick);
                    }
                }
            }
        }

        for (Brick brick : deadBricks) {
            board.bricks.remove(brick);
            board.scoreManager.updateScore(ScoreManager.ScoreEvent.BRICK_DESTROYED);
            horizontalOverlapBricks.remove(brick);
        }
    }

    private boolean getHorizontalBrickPreCollision(Brick brick) {
        return horizontalOverlapBricks.contains(brick);
    }

    private void catchBalls() {
        List<Ball> lostBalls = new ArrayList<Ball>();
        for (Ball ball : board.getBalls()) {

            Vector2 bounceOffVector = board.getBallCatcher().getBounceOffVector(ball);
            if (!bounceOffVector.isZero()) {
                lostBalls.add(ball);
            }
        }
        for (Ball lostBall : lostBalls) {
            board.getBalls().remove(lostBall);
            board.scoreManager.updateScore(ScoreManager.ScoreEvent.BALL_LOST);
        }
    }

    private static boolean ballObjectOverlap(Ball ball, GameDrawable object, boolean horizontal) { // GameDrawables, man!
        if (horizontal) {

            boolean rightSideClip = ball.getLeft() < object.getRight()
                    && ball.getLeft() > object.getLeft();
            boolean leftSideClip = ball.getRight() > object.getLeft()
                    && ball.getRight() < object.getRight();
            return rightSideClip || leftSideClip;

        } else {

            boolean upSideClip = ball.getDown() < object.getUp()
                    && ball.getDown() > object.getDown();
            boolean downSideClip = ball.getUp() > object.getDown()
                    && ball.getUp() < object.getUp();
            return upSideClip || downSideClip;
        }
    }

    private void fixOverlap(GameDrawable mobile, GameDrawable immobile, boolean horizontalAxis) {
        float moveRight = 0.0f;
        float moveLeft = 0.0f;
        float moveUp = 0.0f;
        float moveDown = 0.0f;
        if (horizontalAxis) {
            moveRight = (mobile.getLeft() < immobile.getRight()
                    && mobile.getLeft() > immobile.getLeft()) ? immobile.getRight() - mobile.getLeft() : 0.0f;
            moveLeft = (mobile.getRight() > immobile.getLeft()
                    && mobile.getRight() < immobile.getRight()) ? mobile.getRight() - immobile.getLeft() : 0.0f;
        } else {
            moveUp = (mobile.getDown() < immobile.getUp()
                    && mobile.getDown() > immobile.getDown()) ? immobile.getUp() - mobile.getDown() : 0.0f;
            moveDown = (mobile.getUp() > immobile.getDown()
                    && mobile.getUp() < immobile.getUp()) ? mobile.getUp() - immobile.getDown() : 0.0f;
        }
        mobile.setPosition(new Vector2(mobile.getPosition().x + moveRight - moveLeft,
                mobile.getPosition().y + moveUp - moveDown));
    }
}
