package oop.arkanoid;

enum RecognizedKeys {
    LEFT(21), RIGHT(22), SPACE(62),
    UP(19), DOWN(20), ENTER(66), ESC(131), NULL(-1);

    private final int keycode;

    RecognizedKeys(int keycode) {
        this.keycode = keycode;
    }

    static RecognizedKeys getEnum(int keycode) {
        for (RecognizedKeys key : RecognizedKeys.values()) {
            if (key.keycode == keycode) {
                return key;
            }
        }
        return NULL;
    }
}
