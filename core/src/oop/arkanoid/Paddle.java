package oop.arkanoid;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

class Paddle extends GameDrawable {
    private static final float maxSpeed = 1.0f;

    private static final Vector2 velocityStop = new Vector2(0.0f, 0.0f);
    private static final Vector2 velocityLeft = new Vector2(-maxSpeed, 0.0f);
    private static final Vector2 velocityRight = new Vector2(maxSpeed, 0.0f);

    private boolean isLeftThrusterOn, isRightThrusterOn;

    Paddle() {
        size = new Vector2(50.0f, 20.0f);
        position = new Vector2(0.0f, 0.0f);
        isLeftThrusterOn = isRightThrusterOn = false;
    }

    void turnLeftThrusterOn() {
        isLeftThrusterOn = true;
    }

    void turnLeftThrusterOff() {
        isLeftThrusterOn = false;
    }

    void turnRightThrusterOn() {
        isRightThrusterOn = true;
    }

    void turnRightThrusterOff() {
        isRightThrusterOn = false;
    }

    private Vector2 getVelocity() {
        if (isLeftThrusterOn && !isRightThrusterOn) {
            return new Vector2(velocityLeft);
        } else if (isRightThrusterOn && !isLeftThrusterOn) {
            return new Vector2(velocityRight);
        } else
            return new Vector2(velocityStop);
    }

    @Override
    public void update(long delta) {
        position.add(getVelocity().scl(delta));
    }

    @Override
    Texture getTexture() {
        return textureMan.getTexture(this);
    }
}
