package oop.arkanoid;

import com.badlogic.gdx.math.Vector2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BallTest {
    private Ball testBall;

    @BeforeEach
    void setUp() {
        Paddle paddlePrime = new Paddle();
        testBall = new Ball(paddlePrime);
    }

    @Test
    void getVelocity() {
        final Vector2 zeroVelocity = new Vector2(0.0f, 0.0f);
        final Vector2 startVelocity = new Vector2(0.25f, 0.25f);

        assertEquals(testBall.getVelocity(), zeroVelocity);
        testBall.putInMotion();
        assertEquals(testBall.getVelocity(), startVelocity);
    }

    @Test
    void getDamage() {
        final int stardardBallDamage = 1;

        assertEquals(testBall.getDamage(), stardardBallDamage);
    }

    @Test
    void update() {
        final Vector2 initialPosition = testBall.getPosition();
        assertEquals(testBall.position, initialPosition);

        testBall.update(0);
        assertEquals(testBall.position, initialPosition);

        testBall.update(100);
        assertEquals(testBall.position, initialPosition);

        testBall.putInMotion();
        testBall.update(0);
        assertEquals(testBall.position, initialPosition);

        testBall.update(10);
        final Vector2 movedPosition = new Vector2(20.0f, 24.5f);
        assertEquals(testBall.position, movedPosition);
    }

    @Test
    void putInMotion() {
        final Vector2 startVector = new Vector2(0.0f, 0.0f);
        assertEquals(testBall.getVelocity(), startVector);

        testBall.putInMotion();
        final Vector2 motionVector = new Vector2(0.25f, 0.25f);
        assertEquals(testBall.getVelocity(), motionVector);
    }

    @Test
    void reflect() {
        testBall.putInMotion();
        testBall.reflect(false);
        final Vector2 firstReflect = new Vector2(-0.25f, 0.25f);
        assertEquals(testBall.getVelocity(), firstReflect);

        testBall.reflect(true);
        final Vector2 secondReflect = new Vector2(-0.25f, -0.25f);
        assertEquals(testBall.getVelocity(), secondReflect);
    }
}