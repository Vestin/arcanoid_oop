package oop.arkanoid;

import com.badlogic.gdx.math.Vector2;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoundaryTest {
    private final Boundary leftWallLeft = new Boundary(Boundary.Orientation.Vertical, Boundary.ForbiddenSide.Lesser, 0);
    private final Boundary leftWallRight = new Boundary(Boundary.Orientation.Vertical, Boundary.ForbiddenSide.Greater, 0);
    private final Boundary rightWallLeft = new Boundary(Boundary.Orientation.Vertical, Boundary.ForbiddenSide.Lesser, 640);
    private final Boundary rightWallRight = new Boundary(Boundary.Orientation.Vertical, Boundary.ForbiddenSide.Greater, 640);
    private final Boundary topWallUp = new Boundary(Boundary.Orientation.Horizontal, Boundary.ForbiddenSide.Greater, 480);
    private final Boundary topWallDown = new Boundary(Boundary.Orientation.Horizontal, Boundary.ForbiddenSide.Lesser, 480);

    private final Brick centralBrick = new Brick(new Vector2(240.0f, 320.0f));
    private final Brick midTopBrick = new Brick(new Vector2(240.0f, 475.0f));
    private final Brick topBrick = new Brick(new Vector2(240.0f, 1000.0f));
    private final Brick midLeftBrick = new Brick(new Vector2(-5.0f, 320.0f));
    private final Brick leftBrick = new Brick(new Vector2(-1000.0f, 320.0f));
    private final Brick midRightBrick = new Brick(new Vector2(475.0f, 320.0f));
    private final Brick rightBrick = new Brick(new Vector2(1000.0f, 320.0f));



    @Test
    void getConstrainVector(){
        // LEFT LEFT
        final Vector2 leftLeftCentral = leftWallLeft.getConstrainVector(centralBrick);
        assertTrue(leftLeftCentral.isZero());
        final Vector2 leftLeftmidTop = leftWallLeft.getConstrainVector(midTopBrick);
        assertTrue(leftLeftmidTop.isZero());
        final Vector2 leftLeftTop = leftWallLeft.getConstrainVector(topBrick);
        assertTrue(leftLeftTop.isZero());
        final Vector2 leftLeftMidLeft = leftWallLeft.getConstrainVector(midLeftBrick);
        assertFalse(leftLeftMidLeft.isZero());
        final Vector2 leftLeftLeft = leftWallLeft.getConstrainVector(leftBrick);
        assertFalse(leftLeftLeft.isZero());
        final Vector2 leftLeftMidRight = leftWallLeft.getConstrainVector(midRightBrick);
        assertTrue(leftLeftMidRight.isZero());
        final Vector2 leftLeftRight = leftWallLeft.getConstrainVector(rightBrick);
        assertTrue(leftLeftRight.isZero());

        // LEFT RIGHT
        final Vector2 leftRightCentral = leftWallRight.getConstrainVector(centralBrick);
        assertFalse(leftRightCentral.isZero());
        final Vector2 leftRightmidTop = leftWallRight.getConstrainVector(midTopBrick);
        assertFalse(leftRightmidTop.isZero());
        final Vector2 leftRightTop = leftWallRight.getConstrainVector(topBrick);
        assertFalse(leftRightTop.isZero());
        final Vector2 leftRightMidLeft = leftWallRight.getConstrainVector(midLeftBrick);
        assertFalse(leftRightMidLeft.isZero());
        final Vector2 leftRightLeft = leftWallRight.getConstrainVector(leftBrick);
        assertTrue(leftRightLeft.isZero());
        final Vector2 leftRightMidRight = leftWallRight.getConstrainVector(midRightBrick);
        assertFalse(leftRightMidRight.isZero());
        final Vector2 leftRightRight = leftWallRight.getConstrainVector(rightBrick);
        assertFalse(leftRightRight.isZero());
        
        // RIGHT LEFT
        final Vector2 rightleftCentral = rightWallLeft.getConstrainVector(centralBrick);
        assertFalse(rightleftCentral.isZero());
        final Vector2 rightleftmidTop = rightWallLeft.getConstrainVector(midTopBrick);
        assertFalse(rightleftmidTop.isZero());
        final Vector2 rightleftTop = rightWallLeft.getConstrainVector(topBrick);
        assertFalse(rightleftTop.isZero());
        final Vector2 rightleftMidLeft = rightWallLeft.getConstrainVector(midLeftBrick);
        assertFalse(rightleftMidLeft.isZero());
        final Vector2 rightleftLeft = rightWallLeft.getConstrainVector(leftBrick);
        assertFalse(rightleftLeft.isZero());
        final Vector2 rightleftMidRight = rightWallLeft.getConstrainVector(midRightBrick);
        assertFalse(rightleftMidRight.isZero());
        final Vector2 rightleftRight = rightWallLeft.getConstrainVector(rightBrick);
        assertTrue(rightleftRight.isZero());
        
        // RIGHT RIGHT
        final Vector2 rightRightCentral = rightWallRight.getConstrainVector(centralBrick);
        assertTrue(rightRightCentral.isZero());
        final Vector2 rightRightmidTop = rightWallRight.getConstrainVector(midTopBrick);
        assertTrue(rightRightmidTop.isZero());
        final Vector2 rightRightTop = rightWallRight.getConstrainVector(topBrick);
        assertTrue(rightRightTop.isZero());
        final Vector2 rightRightMidLeft = rightWallRight.getConstrainVector(midLeftBrick);
        assertTrue(rightRightMidLeft.isZero());
        final Vector2 rightRightLeft = rightWallRight.getConstrainVector(leftBrick);
        assertTrue(rightRightLeft.isZero());
        final Vector2 rightRightMidRight = rightWallRight.getConstrainVector(midRightBrick);
        assertTrue(rightRightMidRight.isZero());
        final Vector2 rightRightRight = rightWallRight.getConstrainVector(rightBrick);
        assertFalse(rightRightRight.isZero());
        
        // TOP UP
        final Vector2 topUpCentral = topWallUp.getConstrainVector(centralBrick);
        assertTrue(topUpCentral.isZero());
        final Vector2 topUpmidTop = topWallUp.getConstrainVector(midTopBrick);
        assertFalse(topUpmidTop.isZero());
        final Vector2 topUpTop = topWallUp.getConstrainVector(topBrick);
        assertFalse(topUpTop.isZero());
        final Vector2 topUpMidLeft = topWallUp.getConstrainVector(midLeftBrick);
        assertTrue(topUpMidLeft.isZero());
        final Vector2 topUpLeft = topWallUp.getConstrainVector(leftBrick);
        assertTrue(topUpLeft.isZero());
        final Vector2 topUpMidRight = topWallUp.getConstrainVector(midRightBrick);
        assertTrue(topUpMidRight.isZero());
        final Vector2 topUpRight = topWallUp.getConstrainVector(rightBrick);
        assertTrue(topUpRight.isZero());
        
        // TOP DOWN
        final Vector2 topDownCentral = topWallDown.getConstrainVector(centralBrick);
        assertFalse(topDownCentral.isZero());
        final Vector2 topDownmidTop = topWallDown.getConstrainVector(midTopBrick);
        assertFalse(topDownmidTop.isZero());
        final Vector2 topDownTop = topWallDown.getConstrainVector(topBrick);
        assertTrue(topDownTop.isZero());
        final Vector2 topDownMidLeft = topWallDown.getConstrainVector(midLeftBrick);
        assertFalse(topDownMidLeft.isZero());
        final Vector2 topDownLeft = topWallDown.getConstrainVector(leftBrick);
        assertFalse(topDownLeft.isZero());
        final Vector2 topDownMidRight = topWallDown.getConstrainVector(midRightBrick);
        assertFalse(topDownMidRight.isZero());
        final Vector2 topDownRight = topWallDown.getConstrainVector(rightBrick);
        assertFalse(topDownRight.isZero());
        
    }
    @Test
    void getBounceOffVector() {
        // LEFT LEFT
        final Vector2 leftLeftCentral = leftWallLeft.getBounceOffVector(centralBrick);
        assertTrue(leftLeftCentral.isZero());
        final Vector2 leftLeftmidTop = leftWallLeft.getBounceOffVector(midTopBrick);
        assertTrue(leftLeftmidTop.isZero());
        final Vector2 leftLeftTop = leftWallLeft.getBounceOffVector(topBrick);
        assertTrue(leftLeftTop.isZero());
        final Vector2 leftLeftMidLeft = leftWallLeft.getBounceOffVector(midLeftBrick);
        assertFalse(leftLeftMidLeft.isZero());
        final Vector2 leftLeftLeft = leftWallLeft.getBounceOffVector(leftBrick);
        assertFalse(leftLeftLeft.isZero());
        final Vector2 leftLeftMidRight = leftWallLeft.getBounceOffVector(midRightBrick);
        assertTrue(leftLeftMidRight.isZero());
        final Vector2 leftLeftRight = leftWallLeft.getBounceOffVector(rightBrick);
        assertTrue(leftLeftRight.isZero());

        // LEFT RIGHT
        final Vector2 leftRightCentral = leftWallRight.getBounceOffVector(centralBrick);
        assertFalse(leftRightCentral.isZero());
        final Vector2 leftRightmidTop = leftWallRight.getBounceOffVector(midTopBrick);
        assertFalse(leftRightmidTop.isZero());
        final Vector2 leftRightTop = leftWallRight.getBounceOffVector(topBrick);
        assertFalse(leftRightTop.isZero());
        final Vector2 leftRightMidLeft = leftWallRight.getBounceOffVector(midLeftBrick);
        assertFalse(leftRightMidLeft.isZero());
        final Vector2 leftRightLeft = leftWallRight.getBounceOffVector(leftBrick);
        assertTrue(leftRightLeft.isZero());
        final Vector2 leftRightMidRight = leftWallRight.getBounceOffVector(midRightBrick);
        assertFalse(leftRightMidRight.isZero());
        final Vector2 leftRightRight = leftWallRight.getBounceOffVector(rightBrick);
        assertFalse(leftRightRight.isZero());

        // RIGHT LEFT
        final Vector2 rightleftCentral = rightWallLeft.getBounceOffVector(centralBrick);
        assertFalse(rightleftCentral.isZero());
        final Vector2 rightleftmidTop = rightWallLeft.getBounceOffVector(midTopBrick);
        assertFalse(rightleftmidTop.isZero());
        final Vector2 rightleftTop = rightWallLeft.getBounceOffVector(topBrick);
        assertFalse(rightleftTop.isZero());
        final Vector2 rightleftMidLeft = rightWallLeft.getBounceOffVector(midLeftBrick);
        assertFalse(rightleftMidLeft.isZero());
        final Vector2 rightleftLeft = rightWallLeft.getBounceOffVector(leftBrick);
        assertFalse(rightleftLeft.isZero());
        final Vector2 rightleftMidRight = rightWallLeft.getBounceOffVector(midRightBrick);
        assertFalse(rightleftMidRight.isZero());
        final Vector2 rightleftRight = rightWallLeft.getBounceOffVector(rightBrick);
        assertTrue(rightleftRight.isZero());

        // RIGHT RIGHT
        final Vector2 rightRightCentral = rightWallRight.getBounceOffVector(centralBrick);
        assertTrue(rightRightCentral.isZero());
        final Vector2 rightRightmidTop = rightWallRight.getBounceOffVector(midTopBrick);
        assertTrue(rightRightmidTop.isZero());
        final Vector2 rightRightTop = rightWallRight.getBounceOffVector(topBrick);
        assertTrue(rightRightTop.isZero());
        final Vector2 rightRightMidLeft = rightWallRight.getBounceOffVector(midLeftBrick);
        assertTrue(rightRightMidLeft.isZero());
        final Vector2 rightRightLeft = rightWallRight.getBounceOffVector(leftBrick);
        assertTrue(rightRightLeft.isZero());
        final Vector2 rightRightMidRight = rightWallRight.getBounceOffVector(midRightBrick);
        assertTrue(rightRightMidRight.isZero());
        final Vector2 rightRightRight = rightWallRight.getBounceOffVector(rightBrick);
        assertFalse(rightRightRight.isZero());

        // TOP UP
        final Vector2 topUpCentral = topWallUp.getBounceOffVector(centralBrick);
        assertTrue(topUpCentral.isZero());
        final Vector2 topUpmidTop = topWallUp.getBounceOffVector(midTopBrick);
        assertFalse(topUpmidTop.isZero());
        final Vector2 topUpTop = topWallUp.getBounceOffVector(topBrick);
        assertFalse(topUpTop.isZero());
        final Vector2 topUpMidLeft = topWallUp.getBounceOffVector(midLeftBrick);
        assertTrue(topUpMidLeft.isZero());
        final Vector2 topUpLeft = topWallUp.getBounceOffVector(leftBrick);
        assertTrue(topUpLeft.isZero());
        final Vector2 topUpMidRight = topWallUp.getBounceOffVector(midRightBrick);
        assertTrue(topUpMidRight.isZero());
        final Vector2 topUpRight = topWallUp.getBounceOffVector(rightBrick);
        assertTrue(topUpRight.isZero());

        // TOP DOWN
        final Vector2 topDownCentral = topWallDown.getBounceOffVector(centralBrick);
        assertFalse(topDownCentral.isZero());
        final Vector2 topDownmidTop = topWallDown.getBounceOffVector(midTopBrick);
        assertFalse(topDownmidTop.isZero());
        final Vector2 topDownTop = topWallDown.getBounceOffVector(topBrick);
        assertTrue(topDownTop.isZero());
        final Vector2 topDownMidLeft = topWallDown.getBounceOffVector(midLeftBrick);
        assertFalse(topDownMidLeft.isZero());
        final Vector2 topDownLeft = topWallDown.getBounceOffVector(leftBrick);
        assertFalse(topDownLeft.isZero());
        final Vector2 topDownMidRight = topWallDown.getBounceOffVector(midRightBrick);
        assertFalse(topDownMidRight.isZero());
        final Vector2 topDownRight = topWallDown.getBounceOffVector(rightBrick);
        assertFalse(topDownRight.isZero());
    }
}